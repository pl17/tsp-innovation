# Test Specification for Innovation Group #

Description of task at `/TSP.md`.

## Recommended ##

PHP: Use it with 7.0 or upper also you install library CURL.

Project developing and testing with version `PHP 7.0.22`, `PHP 7.1.11-1`.



## Installing ##

For preparing project for use just install Autoloader's package via composer and set permission to file
`scannerImages` for execution.

Below example of commands for use Composer what had been installed globally.

```bash
composer install
chmod +x scannerImages
``` 

## Using ##

For run scanning just types path to script `scannerImages` and http link to site for scanning.

Example:

```bash
./scannerImages ya.ru
```

 * P.S.: when type ya.ru it will scans only one page.
 Reason in that all parsed urls point to yandex.ru.
 
 * P.P.S.: To the script added limit for page what will completed scan.
 You can change it at 
  