<?php
/**
 * Project:     tsp-innovation
 * File:        /src/ScannerImage.php
 * Author:      NL planet17 <7.fast.cookies@gmail.com>
 * DateTime:    M11.D26.2017 8:37 PM
 */

namespace App;

use App\Crawler\URL;
use App\Reporter\HtmlGenerator;
use App\Crawler\WebCrawler as Crawler;
use Exception as E;

/**
 * Class ScannerImage
 *
 * @package App
 *
 * @author  NL planet17 <7.fast.cookies@gmail.com>
 */
class ScannerImage extends Application\Cli
{
    /**
     * Value what will be set to php's MAX_EXECUTION_TIME.
     *
     * @const MAX_EXECUTION_TIME
     */
    const MAX_EXECUTION_TIME = 200;


    /**
     * Value means limit of pages numbers what will be scans.
     *
     * @const LIMIT_FOR_SCAN_PAGES
     */
    const LIMIT_FOR_SCAN_PAGES = 50;


    /**
     * Property contains link to site for scan.
     *
     * @var URL $url
     */
    protected $url = null;


    /**
     * Class Reporter what will generate reports.
     *
     * @var HtmlGenerator
     */
    protected $reporter;


    /**
     * @var Crawler
     */
    protected $crawler;


    /**
     * @return integer
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    public function run()
    {
        /* set path for reports and construct Reporter */
        $ds = DIRECTORY_SEPARATOR;
        $path = __DIR__ . $ds . '..' . $ds . 'reports';
        $this->reporter = new HtmlGenerator($path);

        ini_set('max_execution_time', self::MAX_EXECUTION_TIME);

        return parent::run();
    }


    /**
     * @throws E
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    public function preparingArgv()
    {
        global $argv;
        $target = $argv[1] ?? null;

        $this->url = new URL($target);

        if ( !$this->url) {
            throw new E(self::ERR_MSG_NON_VALID_LINK);
        }
    }


    /**
     * @inheritdoc
     */
    public function executeCommand()
    {
        $this->crawler = new Crawler($this->url);
        $this->crawler->setLimitForScanningPages(self::LIMIT_FOR_SCAN_PAGES);
        $result = $this->crawler->crawling();
        $this->reporter->report($result);
    }
}
