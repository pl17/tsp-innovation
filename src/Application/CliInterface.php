<?php
/**
 * Project:     tsp-innovation
 * File:        /src/CliApplicationInterface.php
 * Author:      NL planet17 <7.fast.cookies@gmail.com>
 * DateTime:    M11.D26.2017 8:41 PM
 */


namespace App\Application;

/**
 * Interface CliApplicationInterface
 *
 * @package App
 *
 * @author  NL planet17 <7.fast.cookies@gmail.com>
 */
interface CliInterface
{
    /**
     * Method must implement custom processing of provided params.
     * @return void
     */
    public function preparingArgv();


    /**
     * Method must implement custom execution of Application.
     * @return void
     */
    public function executeCommand();
}
