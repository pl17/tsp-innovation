<?php
/**
 * Project:     tsp-innovation
 * File:        /src/CliApplication.php
 * Author:      NL planet17 <7.fast.cookies@gmail.com>
 * DateTime:    M11.D26.2017 6:13 PM
 */

namespace App\Application;

use Exception as E;

/**
 * Class Application
 *
 * @package App
 *
 * @author  NL planet17 <7.fast.cookies@gmail.com>
 */
abstract class Cli implements CliInterface
{
    /**
     * Error message when init script not from CLI.
     *
     * @const ERR_MSG_NON_CLI
     */
    const ERR_MSG_NON_CLI = 'Application can use only from CLI environment.';


    /**
     * Error message on not valid link.
     *
     * @const ERR_MSG_NON_VALID_LINK
     */
    const ERR_MSG_NON_VALID_LINK = 'Something wrong with provided link. You must provide valid link.';

    /**
     * Value contains at self::$status while execution of App still good.
     * @const STATUS_WELL
     */
    const STATUS_WELL = 0;

    /**
     * Value contains at self::$status when execution of App had been called some Execution.
     * @const STATUS_ERROR
     */
    const STATUS_ERROR = 1;

    /**
     * Status of execution what will be return to shell.
     *
     * 0 - is successful.
     *
     * 1 - is error.
     *
     * @var integer
     */
    protected $status = self::STATUS_WELL;

    /**
     * Property contain error message whether something wrong.
     *
     * @var string
     */
    protected $eMsg = '';


    /**
     * CliApplication constructor.
     *
     * @throws E
     */
    public function __construct()
    {
        try {
            if ( !$this->isCli()) {
                throw new E(self::ERR_MSG_NON_CLI);
            }
        } catch (E $e) {
            $this->status = self::STATUS_ERROR;
            $this->eMsg = $e->getMessage();
        }
    }


    /**
     * Method checks whether current environment is `CLI`.
     *
     * @return boolean
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    protected function isCli()
    {
        return php_sapi_name() === 'cli';
    }


    /**
     * Method run Application's execution.
     *
     * It call preparingArgv() and executeCommand(),
     * what must been implemented.
     *
     * @return integer
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    public function run()
    {
        try {
            if ($this->status === self::STATUS_WELL) {
                $this->preparingArgv();
                $this->executeCommand();
            } else {
                throw new E($this->eMsg);
            }
        } catch (E $e) {
            $this->status = self::STATUS_ERROR;
            $this->eMsg = $e->getMessage();
        }

        return $this->onExit();
    }


    /**
     * Method will be execute after all other scripts.
     *
     * It return 0 or 1 and print error message.
     *
     * @return int
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    public function onExit()
    {

        if ($this->status !== 0) {
            echo $this->eMsg;
            echo "\n";
        }

        return $this->status;
    }


    /** @inheritdoc */
    abstract public function preparingArgv();


    /** @inheritdoc */
    abstract public function executeCommand();
}
