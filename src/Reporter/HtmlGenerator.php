<?php
/**
 * Project:     tsp-innovation
 * File:        ReportContructor.php
 * Author:      NL planet17 <7.fast.cookies@gmail.com>
 * DateTime:    M11.D26.2017 8:08 PM
 */

namespace App\Reporter;

use Exception as E;

/**
 * Class ReportConstructor
 *
 * @package App
 *
 * @author  NL planet17 <7.fast.cookies@gmail.com>
 */
class HtmlGenerator
{
    const ERR_MSG_NOT_DIR = 'Provide correct path to directory.';


    const ERR_MSG_DIR_NON_WRITABLE = 'Had been set not writable directory.';


    const REPORTS_FILE_NAME_PREFIX = 'report_';


    const REPORTS_FILE_EXTENSION = 'html';

    /**
     * HTML Templates.
     */
    const HTML_TEMPLATE_START_OF_FILE = '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>Report</title></head><body><table border="1">';
    const HTML_TEMPLATE_TABLE_HEADER = '<tr><th>Url</th><th>Number of &lt;IMG&gt; tags</th><th>Processing time</th></tr>';
    const HTML_TEMPLATE_END_OF_FILE = '</table></body></html>';

    /**
     * @var string
     */
    public $pathToReportsDirectory = '';

    /**
     * @var string
     */
    public $fileName;


    /**
     * HtmlGenerator constructor.
     *
     * @param $path
     *
     * @throws E
     */
    public function __construct($path)
    {
        if ( !is_dir($path)) {
            throw new E(self::ERR_MSG_NOT_DIR);
        }

        if ( !is_writable($path)) {
            throw new E(self::ERR_MSG_DIR_NON_WRITABLE);
        }

        $this->pathToReportsDirectory = $path;
    }


    /**
     * @param array $data
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    public function report(array $data)
    {
        $this->fileName = $this->generateFileName();
        $this->checkPermissionsOnFile();

        $data = $this->forgeData($data);
        $data = $this->sortData($data);
        $content = $this->prepareContent($data);
        file_put_contents($this->fileName, $content);
    }


    protected function checkPermissionsOnFile()
    {
        if ( !is_writable($this->fileName)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Just generate correct name for file like at rules (`TSP.md`).
     *
     * @return string
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    protected function generateFileName(): string
    {
        $ext = '.' . self::REPORTS_FILE_EXTENSION;
        $name = $this->pathToReportsDirectory . DIRECTORY_SEPARATOR;
        $name .= self::REPORTS_FILE_NAME_PREFIX . date('d.m.Y') . $ext;

        return $name;
    }


    /**
     * Method prepare data to right structure.
     *
     * @param array $data
     *
     * @return array
     */
    protected function forgeData(array $data): array
    {
        foreach ($data as $url => $row) {
            $data[$url]['url'] = $url;
        }

        return array_values($data);
    }


    /**
     * Method sort prepared data in order by `count` desc.
     *
     * @param array $data
     *
     * @return array
     */
    protected function sortData(array $data): array
    {
        usort($data, function ($a, $b) {
            $a = intval($a['count']);
            $b = intval($b['count']);

            return ($a == $b) ? 0 : (($a < $b) ? 1 : -1);
        });

        return $data;
    }


    protected function prepareContent(array $data)
    {
        $content = self::HTML_TEMPLATE_START_OF_FILE;
        $content .= self::HTML_TEMPLATE_TABLE_HEADER;
        foreach ($data as $url => $row) {
            $content .= "\n\t<tr><td>{$row['url']}</td><td>{$row['count']}</td><td>{$row['time']}</td></tr>";
        }
        $content .= self::HTML_TEMPLATE_END_OF_FILE;

        return $content;
    }
}
