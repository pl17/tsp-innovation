<?php
/**
 * Project:     tsp-innovation
 * File:        /src/Crawler/WebCrawler.php
 * Author:      NL planet17 <7.fast.cookies@gmail.com>
 * DateTime:    M11.D26.2017 9:17 PM
 */

namespace App\Crawler;

use DOMDocument as DOMDoc;
use Exception as E;

/**
 * Class WebCrawler
 *
 * @package App\Crawler
 *
 * @author  NL planet17 <7.fast.cookies@gmail.com>
 *
 */
class WebCrawler
{
    /**
     * @var \App\Crawler\URL
     */
    protected $url;

    /**
     * Property contain value of pages number what will be scan.
     * @var integer $limit
     */
    protected $limit;

    /**
     * Property must contain assoc array of response with visited pages.
     *
     * Keys of array means url.
     *
     * Value of element it boolean (for not well processed page), or
     * array with response (for completed page).
     *
     * @var array
     */
    protected $visitedLinks = [];

    /**
     * Property contain assoc array with urls what had been parsed from page,
     * and will be processed in few moments.
     *
     * @var array
     */
    protected $foundedLinks = [];


    /**
     * WebCrawler constructor.
     *
     * @param \App\Crawler\URL $URL
     * @param int              $limit
     */
    public function __construct(URL $URL, int $limit = 1000)
    {
        $this->url = $URL;
        $this->limit = $limit;
    }


    /**
     * Set limit for number of pages what will be scan.
     *
     * @param integer $limit - Only positive numbers.
     */
    public function setLimitForScanningPages(int $limit)
    {
        ($limit) && $this->limit = $limit;
    }


    /**
     * Method init main process with parsing provided site.
     * @return array
     */
    public function crawling()
    {
        /* visit first provided url. */
        $this->visitURL($this->url->getFullUrl());
        /* go to others parsed link */
        while ($this->foundedLinks && $this->limit--) {
            /* get some key */
            $link = key($this->foundedLinks);
            try {
                $this->visitURL($link);
                /* reset key for any time we get first */
                reset($this->foundedLinks);
                usleep(45);
            } catch (E $e) {
            }
        }

        return $this->visitedLinks;
    }


    /**
     * Method parse url, get info and init initialize of urls analyzing and collect other data.
     *
     * That method contain calls microtime() for bench time of execution.
     *
     * @param string $url
     *
     * @return boolean
     */
    protected function visitURL(string $url)
    {
        $urls = [];

        $time = microtime(true);

        $dom = new DOMDoc('1.0');
        $this->markVisitedLink($url);

        /* with preventing displaying warning on new html5 tags */
        libxml_use_internal_errors(true);
        $dom->loadHTMLFile($url);
        libxml_clear_errors();

        $anchors = $dom->getElementsByTagName('a');

        /** @var \DOMElement $anchor */
        foreach ($anchors as $anchor) {
            $href = $anchor->getAttribute('href');
            $href = URLHelper::cleanFragments($href);

            if ( !$href) {
                continue;
            }

            /* construct whether not full url */
            $href = $this->buildURL($href, $url);
            /* pass same urls */
            if ($href === $url) {
                continue;
            }
            /* pass other domain */
            $passByCurrent = $this->sameDomain($href, $url);
            $passByInit = $this->sameDomain($href, $this->url->getFullUrl());
            if ( !($passByCurrent || $passByInit)) {
                continue;
            }

            /* pass duplicates from current page */
            if (in_array($href, $urls)) {
                continue;
            }

            /* add key and any value */
            $urls[$href] = false;
        }

        foreach ($urls as $newUrl => $v) {
            /* pass if visited that url early */
            if (array_key_exists($newUrl, $this->visitedLinks)) {
                continue;
            }

            /* pass if had been added early */
            if (array_key_exists($newUrl, $this->foundedLinks)) {
                continue;
            }

            $this->foundedLinks[$newUrl] = $v;
        }

        $time = microtime(true) - $time;

        $this->visitedLinks[$url] = [
            'time'  => $time,
            'count' => $dom
                ->getElementsByTagName('img')
                ->length,
        ];

        return true;
    }


    /**
     * Method checks parsed url. Whether it not fully method will extended it
     * based on data from origin url.
     *
     *
     * @param string $parsedUrl
     * @param string $origin
     *
     * @return bool|string
     */
    protected function buildURL(string $parsedUrl, string $origin)
    {
        if (substr($parsedUrl, 0, 4) === 'http') {
            return $parsedUrl;
        }

        if (substr($parsedUrl, 0, 2) === '//') {
            $parsedUrl = substr($parsedUrl, 2);
            if (substr($parsedUrl, 0, 4) !== 'http') {
                $parsedUrl = $this->url->getScheme() . URLHelper::SCHEME_SEPARATOR . $parsedUrl;
            }
        }

        if (substr($parsedUrl, 0, 1) === '/') {
            $base = URLHelper::getCleanedFromQuery($origin);
            $parsedUrl = $base . $parsedUrl;
        }

        /* check again for relative urls without "/" at start */
        if (substr($parsedUrl, 0, 4) !== 'http') {
            $end = ($parsedUrl[0] === '/');
            $start = ($origin[strlen($origin)] === '/');
            $slash = ($end || $start) ? '' : '/';

            $parsedUrl = $origin . $slash . $parsedUrl;
        }

        return $parsedUrl;
    }


    /**
     * Method compare if $new url relative to $origin.
     *
     * It checks whether $new contain $origin into itself.
     *
     * Checks contains for sub-domain.
     *
     * @param string $new
     * @param string $origin
     *
     * @return boolean
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    protected function sameDomain(string $new, string $origin): bool
    {
        $newParsed = parse_url($new);
        /* More secure for parsed urls than prepared before origin url. */
        $newDomain = $newParsed['host'] ?? '';

        $originDomain = parse_url($origin)['host'];

        return boolval((strpos($newDomain, $originDomain) !== false));
    }


    /**
     * Method transfer link from storage of Founded to Visited.
     *
     * @param string $link
     *
     * @see WebCrawler::$visitedLinks
     * @see WebCrawler::$foundedLinks
     */
    protected function markVisitedLink(string $link)
    {
        $this->visitedLinks[$link] = null;
        unset($this->foundedLinks[$link]);
    }
}
