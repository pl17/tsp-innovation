<?php
/**
 * Project:     tsp-innovation
 * File:        /src/Crawler/URL.php
 * Author:      NL planet17 <7.fast.cookies@gmail.com>
 * DateTime:    M11.D29.2017 11:29 PM
 */

namespace App\Crawler;

use Exception as E;

/**
 * Class URL
 *
 * @package App\Crawler
 */
class URL
{
    /**
     * Property contain scheme of url.
     *
     * @var $scheme
     */
    protected $scheme;

    /**
     * Property contain only domain or full part of url without scheme.
     *
     * @var $domain
     */
    protected $domain;


    /**
     * @const ERR_MSG_WRONG_URL
     */
    const ERR_MSG_WRONG_URL = 'Provided URL wrong!';


    /**
     * @const ERR_MSG_URL_NOT_WORKS
     */
    const ERR_MSG_URL_NOT_WORKS = 'Current URL does not works!';


    /**
     * URL constructor.
     *
     * Method validate provided `url`.
     *
     * @param string $url
     *
     * @throws E
     */
    public function __construct(string $url)
    {
        if (!is_string($url)) {
            throw new E(self::ERR_MSG_WRONG_URL);
        }

        $url = trim($url);
        if (!$url) {
            throw new E(self::ERR_MSG_WRONG_URL);
        }

        list($scheme, $url) = URLHelper::separateUrlScheme($url);

        $this->domain = $url;
        unset($url);

        if (!$scheme) {
            /* Fix $scheme automatically if it does not set */
            foreach (URLHelper::ALLOWED_SCHEMES as $i) {
                $url = $i . $this->getFullUrl();
                if ($this->initRequest($url)) {
                    $scheme = $i;
                    break;
                }
            }

            if (!$scheme) {
                throw new E(self::ERR_MSG_URL_NOT_WORKS);
            }

        } else {
            /* Check whether it works */
            $url = $scheme . $this->getFullUrl();
            if (!$this->initRequest($url)) {
                throw new E(self::ERR_MSG_URL_NOT_WORKS);
            }
        }

        $this->scheme = $scheme;

        // todo $this->domain = URLHelper::getClean($url);

        return $this;
    }


    /**
     * Method return string with full url.
     *
     * @return string - Construct full url from parts.
     */
    public function getFullUrl(): string
    {
        return $this->scheme . URLHelper::SCHEME_SEPARATOR . $this->domain;
    }


    /**
     * @param string  $url
     *
     * @return boolean
     */
    protected function initRequest(string $url = ''): bool
    {
        !$url && $url = $this->getFullUrl();
        /** @var resource $curl */
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_HTTPHEADER     => ['cache-control: no-cache',],
        ]);

        curl_exec($curl);
        $response = curl_getinfo($curl);
        curl_close($curl);

        $response['http_code'] = $response['http_code'] ?? 0;

        /* check whether response - redirect */
        if ($response['http_code'] > 300 && $response['http_code'] < 400) {
            $target = $response['redirect_url'];

            $diff = strlen($target) - strlen($url);

            if (abs($diff) === 1) {
                $check = ($diff > 0) ? $target : $url;
                if ($check[ strlen($check) - 1 ] === '/') {
                    return true;
                }
            }

            return false;
        }

        if ($response['http_code'] === 200) {
            return true;
        }

        return false;
    }


    /**
     * Method return domain part of url.
     *
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }


    public function getCleanDomain(): string
    {
        $pos = strpos($this->domain, '/');
        if ($pos !== false) {
            return substr($this->domain, 0, $pos);
        }

        return $this->domain;
    }


    /**
     * @return mixed
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    public function getScheme(): string
    {
        return $this->scheme;
    }

}
