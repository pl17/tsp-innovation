<?php
/**
 * Project:     tsp-innovation
 * File:        URLHelper.php
 * Author:      NL planet17 <7.fast.cookies@gmail.com>
 * DateTime:    M11.D29.2017 11:26 PM
 */


namespace App\Crawler;

use Exception as E;

class URLHelper
{
    /**
     * @const ALLOWED_SCHEMES
     */
//    const ALLOWED_SCHEMES = ['https', 'http',];
    const ALLOWED_SCHEMES = ['http', 'https',];


    /**
     * @const SCHEME_SEPARATOR
     */
    const SCHEME_SEPARATOR = '://';


    /**
     * @const ERR_MSG_NOT_ALLOWED_SCHEME
     */
    const ERR_MSG_NOT_ALLOWED_SCHEME = 'Now allowed scheme is only HTTP and HTTPS.';


    /**
     * @param string $url
     *
     * @return array
     * @throws E
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    public static function separateUrlScheme(string $url)
    {
        $scheme = self::searchScheme($url);
        $scheme && $url = substr($url, strlen($scheme . self::SCHEME_SEPARATOR));

        return [$scheme, $url];
    }


    /**
     * @param string $url
     *
     * @return mixed|null|string
     *
     * @throws E
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    protected static function searchScheme(string $url)
    {
        return parse_url($url, PHP_URL_SCHEME);
    }


    /**
     * @return boolean
     *
     * @author NL planet17 <7.fast.cookies@gmail.com>
     */
    public static function isAbsoluteUrl()
    {
        return boolval(true);
    }


    public static function cleanFragments($url)
    {
        $fragmentsStartPos = strpos($url, '#');
        if ($fragmentsStartPos === false) {
            return $url;
        }

        return substr($url, 0, $fragmentsStartPos);
    }


    public static function getCleanedFromQuery($url)
    {
        $parsed = parse_url($url);
        return $parsed['scheme'] . URLHelper::SCHEME_SEPARATOR . $parsed['host'];
    }

    public static function getClean()
    {
        if (0 !== strpos($href, 'http')) {
            if (0 === strpos($href, 'javascript:') || 0 === strpos($href, '#')) {
                return false;
            }
            $path = '/' . ltrim($href, '/');
            if (extension_loaded('http')) {
                $new_href = http_build_url($url, ['path' => $path], HTTP_URL_REPLACE, $parts);
            } else {
                $parts = parse_url($url);
                $new_href = $this->buildUrlFromParts($parts);
                $new_href .= $path;
            }
            // Relative urls... (like ./viewforum.php)
            if (0 === strpos($href, './') && !empty($parts['path'])) {
                // If the path isn't really a path (doesn't end with slash)...
                if (!preg_match('@/$@', $parts['path'])) {
                    $path_parts = explode('/', $parts['path']);
                    array_pop($path_parts);
                    $parts['path'] = implode('/', $path_parts) . '/';
                }

                $new_href = $this->buildUrlFromParts($parts) . $parts['path'] . ltrim($href, './');
            }
            $href = $new_href;
        }
        $href = rtrim($href, '/');
        if ($this->same_host && $this->host != $this->getHostFromUrl($href)) {
            return false;
        }

        return $href;
    }
}